﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using BlogApplication.Autofac;

namespace BlogApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType(typeof(CategoryService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(CategoryRepository)).AsImplementedInterfaces();
            builder.RegisterType(typeof(PostService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(PostRepository)).AsImplementedInterfaces();
            builder.RegisterType(typeof(CommentRepository)).AsImplementedInterfaces();
            builder.RegisterType(typeof(CommentService)).AsImplementedInterfaces();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
