﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlogApplication;
using BlogApplication.Autofac;
using PagedList;

namespace BlogApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoriesController : Controller
    {
        private readonly IService<Category> _service;
        private readonly IService<Post> _postService;

        public CategoriesController(IService<Category> service, IService<Post> postService)
        {
            _service = service;
            _postService = postService;
        }

        // GET: Categories
        public ActionResult Index(int page = 1)
        {
            return View(_service.ListAll().ToPagedList(page, 10));
        }

        // GET: Categories/Create      
        public ActionResult Create()
        {
            return View();
        }


        // POST: Categories/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] Category category)
        {
            if (!ModelState.IsValid) return View(category);

            _service.Add(category);
            return RedirectToAction("Index");
        }

        // GET: Categories/Edit/id
        public ActionResult Edit(Guid id)
        {            
            return View(_service.FindById(id));
        }

        // POST: Categories/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Category category)
        {
            if (!ModelState.IsValid) return View(category);

            _service.Update(category);         
            return RedirectToAction("Index");
        }

        // GET: Categories/Delete/id
        public ActionResult Delete(Guid id)
        {            
            var category = _service.FindById(id);            
            ViewBag.Posts = _postService.ListAllCategoryPost(id).Any();                                                   
            return View(category);
        }

        // POST: Categories/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            _service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}