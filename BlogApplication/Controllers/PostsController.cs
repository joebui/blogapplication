﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using BlogApplication;
using BlogApplication.Autofac;
using ImageResizer;
using PagedList;

namespace BlogApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PostsController : Controller
    {
        private readonly IService<Post> _postService;
        private readonly IService<Category> _categoryService;

        public PostsController(IService<Post> postService, IService<Category> categoryService)
        {
            _postService = postService;
            _categoryService = categoryService;
        }

        // GET: Posts
        public ActionResult Index(int page = 1)
        {
            return View(_postService.ListAll().ToPagedList(page, 10));
        }

        public ActionResult Details(Guid id)
        {
            return View(_postService.FindById(id));
        }

        // GET: Posts/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(_categoryService.SelectListValues(), "Id", "Name");
            return View();
        }

        // POST: Posts/Create        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryId,Title,Description,Content")] Post post, HttpPostedFileBase file)
        {            
            if (file == null || file.ContentLength <= 0)
            {
                ModelState.AddModelError("File", "Image file is required");
            }

            if (string.IsNullOrEmpty(post.Title))
            {
                ModelState.AddModelError("Title", "Title is required");                
            }

            if (string.IsNullOrEmpty(post.Description))
            {
                ModelState.AddModelError("Description", "Description is required");
            }

            if (string.IsNullOrEmpty(post.Content))
            {
                ModelState.AddModelError("Content", "Content is required");
            }            

            if (ModelState.IsValid && file != null && file.ContentLength > 0)
            {
                Post.AddPostPhotos(file, post);                                   
                _postService.Add(post);
                return RedirectToAction("Index");
            }
            
            ViewBag.CategoryId = new SelectList(_categoryService.SelectListValues(), "Id", "Name");
            return View(post);
        }

        // GET: Posts/Edit/id
        public ActionResult Edit(Guid id)
        {            
            var post = _postService.FindById(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(_categoryService.SelectListValues(), "Id", "Name", post.CategoryId);
            return View(post);
        }

        // POST: Posts/Edit/id 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                Post.AddPostPhotos(file, post);
            }

            if (string.IsNullOrEmpty(post.Title))
            {
                ModelState.AddModelError("Title", "Title is required");
            }

            if (string.IsNullOrEmpty(post.Description))
            {
                ModelState.AddModelError("Description", "Description is required");
            }

            if (string.IsNullOrEmpty(post.Content))
            {
                ModelState.AddModelError("Content", "Content is required");
            }   

            if (ModelState.IsValid)
            {
                _postService.Update(post);
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(_categoryService.SelectListValues(), "Id", "Name", post.CategoryId);
            return View(post);
        }

        // GET: Posts/Delete/id
        public ActionResult Delete(Guid id)
        {            
            var post = _postService.FindById(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var post = _postService.FindById(id);
            Post.RemoveImageFile(Server.MapPath(post.ThumbnailPath), Server.MapPath(post.ImagePath));
            _postService.Delete(id);            
            return RedirectToAction("Index");
        }               
    }
}
