﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogApplication.Autofac;
using Microsoft.AspNet.Identity;
using PagedList;

namespace BlogApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IService<Category> _categoryService;
        private readonly IService<Post> _postService;
        private readonly IService<Comment> _commentService;

        public HomeController(IService<Post> postService, IService<Comment> commentService, IService<Category> categoryService)
        {
            _postService = postService;
            _commentService = commentService;
            _categoryService = categoryService;
        }

        // GET: Home
        public ActionResult Index()
        {
            return View(_categoryService.ListAll());
        }

        // GET: Post/id        
        public ActionResult Post(Guid id)
        {
            ViewBag.Post = _postService.FindById(id);
            ViewBag.Comments = _commentService.ListAllPostComment(id);
            return View();
        }

        // Post: Post/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Post([Bind(Include = "Content")] Comment comment, Guid id)
        {
            if (ModelState.IsValid)
            {
                comment.PostId = id;
                comment.UserId = User.Identity.GetUserName();
                _commentService.Add(comment);
                return RedirectToAction("Post", "Home", new { id = id });
            }            

            ViewBag.Post = _postService.FindById(id);
            ViewBag.Comments = _commentService.ListAllPostComment(id);
            return View(comment);
        }

        // GET: Category/id        
        public ActionResult Category(Guid id, int page = 1)
        {
            ViewBag.Category = _categoryService.FindById(id);
            ViewBag.Categories = _categoryService.ListAll();
            return View(_postService.ListAllCategoryPost(id).ToPagedList(page, 10));
        }        

        public PartialViewResult Main(int page = 1)
        {            
            return PartialView("_Main", _postService.ListAll().ToPagedList(page, 10));
        }

        [HttpPost]
        public PartialViewResult SearchResult(string title)
        {
            return PartialView("_SearchResult", _postService.FindPostsByTitle(title));
        }
    }
}