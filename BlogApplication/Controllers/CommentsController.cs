﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogApplication.Autofac;

namespace BlogApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CommentsController : Controller
    {
        private readonly IService<Comment> _commentService;

        public CommentsController(IService<Comment> commentService)
        {
            _commentService = commentService;
        }

        // GET: Comments
        public ActionResult Index(Guid id)
        {            
            return View(_commentService.ListAllPostComment(id));
        }

        // GET: Comments/Delete/id
        public ActionResult Delete(Guid id)
        {
            return View(_commentService.FindById(id));
        }

        // POST: Comments/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDelete(Guid id)
        {
            _commentService.Delete(id);
            return RedirectToAction("Index", "Posts");
        }
    }
}