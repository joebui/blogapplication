﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BlogApplication.Autofac
{
    public class CommentService : IService<Comment>
    {
        private readonly BlogEntities _db = new BlogEntities();
        private readonly IRepository<Comment> _repository;

        public CommentService(IRepository<Comment> repository)
        {
            _repository = repository;
        }        

        public IEnumerable<Comment> ListAllPostComment(Guid id)
        {
            return _repository.ListAllPostComment(id);
        }        

        public void Add(Comment t)
        {
            t.Id = Guid.NewGuid();
            t.Created = DateTime.Now;
            _db.Comments.Add(t);
            _db.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var comment = _db.Comments.Find(id);
            _db.Comments.Remove(comment);
            _db.SaveChanges();
        }        

        public Comment FindById(Guid id)
        {
            return _db.Comments.Find(id);
        }

        public void Update(Comment t)
        {
            throw new NotImplementedException();
        }        

        public IEnumerable<Comment> ListAllCategoryPost(Guid id)
        {
            throw new NotImplementedException();
        }        

        public DbSet<Comment> SelectListValues()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comment> FindPostsByTitle(string title)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comment> ListAll()
        {
            throw new NotImplementedException();
        }
    }
}