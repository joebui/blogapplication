﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BlogApplication.Autofac
{
    public class PostRepository : IRepository<Post>
    {
        private readonly BlogEntities _db = new BlogEntities();

        public IEnumerable<Post> ListAll()
        {
            return _db.Posts.OrderByDescending(p => p.Created).ToList();
        }        

        public IEnumerable<Post> ListAllCategoryPost(Guid id)
        {
            return _db.Posts.Where(p => p.CategoryId == id).ToList();
        }

        public IEnumerable<Post> FindPostsByTitle(string title)
        {
            return _db.Posts.Where(p => p.Title.Contains(title)).
                OrderByDescending(p => p.Created).ToList();
        }

        public Post FindById(Guid id)
        {
            return _db.Posts.Find(id);
        }

        public DbSet<Post> SelectListValues()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> ListAllPostComment(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}