﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BlogApplication.Autofac
{
    public class PostService : IService<Post>
    {
        private readonly BlogEntities _db = new BlogEntities();
        private readonly IRepository<Post> _repository;

        public PostService(IRepository<Post> repository)
        {            
            this._repository = repository;
        }

        public IEnumerable<Post> ListAll()
        {
            return _repository.ListAll();
        }        

        public IEnumerable<Post> ListAllCategoryPost(Guid id)
        {
            return _repository.ListAllCategoryPost(id);
        }

        public IEnumerable<Post> FindPostsByTitle(string title)
        {
            return _repository.FindPostsByTitle(title);
        }

        public Post FindById(Guid id)
        {
            return _repository.FindById(id);
        }        

        public void Add(Post t)
        {            
            t.Id = Guid.NewGuid();
            t.Created = DateTime.Now;
            _db.Posts.Add(t);            
            _db.SaveChanges();                      
        }

        public void Update(Post t)
        {
            Debug.WriteLine(t.ThumbnailPath + t.ImagePath + t.Created);
            _db.Entry(t).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var post = _db.Posts.Find(id);            
            _db.DeleteAllCommentsOfPost(id.ToString());
            _db.Posts.Remove(post);       
     
            _db.SaveChanges();
        }

        public DbSet<Post> SelectListValues()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> ListAllPostComment(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}