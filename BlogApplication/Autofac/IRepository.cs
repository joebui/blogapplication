﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BlogApplication.Autofac
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> ListAll();
        IEnumerable<T> ListAllPostComment(Guid id);
        IEnumerable<T> ListAllCategoryPost(Guid id);
        IEnumerable<T> FindPostsByTitle(string title);
        T FindById(Guid id);
        DbSet<T> SelectListValues();
    }
}