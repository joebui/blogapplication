﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BlogApplication.Autofac
{
    public class CommentRepository : IRepository<Comment>
    {
        private readonly BlogEntities _db = new BlogEntities();

        public IEnumerable<Comment> ListAllPostComment(Guid id)
        {
            return _db.Comments.Where(c => c.PostId == id).OrderByDescending(c => c.Created).ToList();
        }

        public IEnumerable<Comment> ListAll()
        {
            throw new NotImplementedException();
        }        

        public IEnumerable<Comment> ListAllCategoryPost(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comment> FindPostsByTitle(string title)
        {
            throw new NotImplementedException();
        }

        public Comment FindById(Guid id)
        {
            throw new NotImplementedException();
        }

        public DbSet<Comment> SelectListValues()
        {
            throw new NotImplementedException();
        }
    }
}