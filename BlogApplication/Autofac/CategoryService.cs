﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BlogApplication.Autofac
{
    public class CategoryService : IService<Category>
    {
        private readonly BlogEntities _db = new BlogEntities();
        private readonly IRepository<Category> _repository;

        public CategoryService(IRepository<Category> repository)
        {
            _repository = repository;
        }

        public IEnumerable<Category> ListAll()
        {
            return _repository.ListAll();
        }        

        public Category FindById(Guid id)
        {
            return _repository.FindById(id);
        }

        public DbSet<Category> SelectListValues()
        {
            return _repository.SelectListValues();
        }

        public void Add(Category t)
        {
            t.Id = Guid.NewGuid();
            _db.Categories.Add(t);
            _db.SaveChanges();
        }

        public void Update(Category t)
        {
            _db.Entry(t).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Delete(Guid id)
        {                        
            var category = _db.Categories.Find(id);
            _db.Categories.Remove(category);
            _db.SaveChanges();
        }

        public IEnumerable<Category> ListAllPostComment(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> ListAllCategoryPost(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> FindPostsByTitle(string title)
        {
            throw new NotImplementedException();
        }
    }
}