﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BlogApplication.Autofac
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly BlogEntities _db = new BlogEntities();

        public IEnumerable<Category> ListAll()
        {
            return _db.Categories.OrderBy(c => c.Name).ToList();
        }        

        public Category FindById(Guid id)
        {
            return _db.Categories.Find(id);
        }

        public DbSet<Category> SelectListValues()
        {
            return _db.Categories;
        }

        public IEnumerable<Category> ListAllPostComment(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> ListAllCategoryPost(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> FindPostsByTitle(string title)
        {
            throw new NotImplementedException();
        }
    }
}