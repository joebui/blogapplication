﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using BlogApplication.Models;
using BlogApplication.Slug;

namespace BlogApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Paging",
                url: "{controller}/Page/{page}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Category",
                "Category/{name}/{id}",
                new { controller = "Home", action = "Category" }
            );

            routes.MapRoute(
                "Post",
                "Post/{title}/{id}",
                new { controller = "Home", action = "Post" }
            );            

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );            
        }
    }
}
