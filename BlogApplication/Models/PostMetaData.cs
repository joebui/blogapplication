﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ImageResizer;

namespace BlogApplication
{
    [MetadataType(typeof(PostMetaData))]
    public partial class Post : IDisposable
    {
        public static void AddPostPhotos(HttpPostedFileBase file, Post post)
        {
            var versions = new Dictionary<string, string>();
            versions.Add("_thumbnail", "width=120&height=120&format=jpg");
            versions.Add("_large", "width=600&maxheight=600&format=jpg");

            foreach (var suffix in versions.Keys)
            {
                var fullName = Path.GetFileNameWithoutExtension(file.FileName);
                var filePath = new StringBuilder();
                filePath.Append("~/Images/");
                filePath.Append(fullName);
                filePath.Append(suffix);
                var filePathString = filePath.ToString();

                switch (suffix)
                {
                    case "_thumbnail":
                        post.ThumbnailPath = filePathString + ".jpg";
                        break;
                    case "_large":
                        post.ImagePath = filePathString + ".jpg";
                        break;
                }

                file.InputStream.Seek(0, SeekOrigin.Begin);
                ImageBuilder.Current.Build(
                    new ImageJob(
                        file.InputStream,
                        filePath.ToString(),
                        new Instructions(versions[suffix]),
                        false,
                        true
                    )
                );
            }
        }

        public string GenerateSlug()
        {
            var phrase = $"{Title}";

            var str = RemoveAccent(phrase).ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            //str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        private string RemoveAccent(string text)
        {
            var bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(text);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public static void RemoveImageFile(string thumbnail, string image)
        {
            File.Delete(thumbnail);
            File.Delete(image);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    public class PostMetaData
    {
        public System.Guid Id { get; set; }
        public System.Guid CategoryId { get; set; }

        [StringLength(200)]
        public string Title { get; set; }          
     
        [AllowHtml]
        public string Content { get; set; }

        [StringLength(500)]
        public string ThumbnailPath { get; set; }

        [StringLength(500)]
        public string ImagePath { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(600)]
        public string Description { get; set; }
    }
}