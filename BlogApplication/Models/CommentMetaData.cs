﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogApplication
{
    [MetadataType(typeof(CommentMetaData))]
    public partial class Comment
    {
         
    }

    public class CommentMetaData
    {        
        public System.Guid Id { get; set; }

        [Required(ErrorMessage = "You forgot to write comment")]
        public string Content { get; set; }

        public Guid? PostId { get; set; }
        public string UserId { get; set; }
        public DateTime? Created { get; set; }
    }
}