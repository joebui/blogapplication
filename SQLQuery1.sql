﻿create table Category (
	Id uniqueidentifier primary key default NEWID(),
	Name nvarchar(100) not null
);

create table Post (
	Id uniqueidentifier primary key default NEWID(),
	CategoryId uniqueidentifier references Category(Id),
	Title nvarchar(200) not null,
	Content text not null
);

create table Comment (
	Id uniqueidentifier primary key default NEWID(),
	Content text not null,
	PostId uniqueidentifier references Post(Id),
	UserId varchar(50) not null
);

create procedure DeleteAllCommentsOfPost
@PostId varchar(50)
as
delete from Comment where PostId = @PostId

create procedure DeleteAllPostsofCategory
@CategoryId varchar(50)
as
delete from Post where CategoryId = @CategoryId